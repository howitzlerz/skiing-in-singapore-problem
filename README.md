##SKi Rental Problem
###The ski rental problem is the name given to a class of problems in which there is a choice between continuing to pay a repeating cost or paying a one-time cost which eliminates or reduces the repeating cost.

###Applications:
1. Caching
2. CP Acknowledgement
3. Scheduling
4. Refactoring