#Class Description: This class represents the grid on the map to where one can possibly ski to.
#Class Properties:
#1. x = x-coordinate
#2. y = y-coordinate
#3. value = elevation of that area
#4. top, bottom, left, right = the neighboring grid where you can ski next
class Ski:
	#Class Initializer
	def __init__(self, x, y, value):
		self.x=x;
		self.y=y;
		self.value=value;
		self.top=None;
		self.bottom=None;
		self.left=None;
		self.right=None;
	
	#Description: Class' string representation
	#Returns: String
	def __str__(self):
		return str(	'Position: ('+str(self.x)+', '+str(self.y)+')\n'+
					'Value: '+str(self.value));
	
	#Description: Returns the ski's neighboring ski value
	#Returns: Array [Top.value, Bottom.value, Left.value, Right.value]
	def get_around_value(self):
		self.around_value=[];
		#Top
		if self.top and self.top.value<self.value:
			self.around_value.append(self.top.value);
		else:
			self.around_value.append(0);
		#Bottom
		if self.bottom and self.bottom.value<self.value:
			self.around_value.append(self.bottom.value);
		else:
			self.around_value.append(0);	
		#Left
		if self.left and self.left.value<self.value:
			self.around_value.append(self.left.value);
		else:
			self.around_value.append(0);
		#Right
		if self.right and self.right.value<self.value:
			self.around_value.append(self.right.value);
		else:
			self.around_value.append(0);
		return self.around_value;
	
	#Description: Returns the ski's neighboring ski object
	#Returns: Array [Top, Bottom, Left, Right]
	def get_around_ski(self):
		self.around_ski=[];
		#Top
		if self.top and self.top.value<self.value:
			self.around_ski.append(self.top);
		else:
			self.around_ski.append(None);
		#Bottom
		if self.bottom and self.bottom.value<self.value:
			self.around_ski.append(self.bottom);
		else:
			self.around_ski.append(None);
		#Left
		if self.left and self.left.value<self.value:
			self.around_ski.append(self.left);
		else:
			self.around_ski.append(None);
		#Right
		if self.right and self.right.value<self.value:
			self.around_ski.append(self.right);
		else:
			self.around_ski.append(None);
		return self.around_ski;
	
	#Description: List can contains list within itself. This will extract the data in each list returning them into a single list of data.
	#Returns: Array
	def extract(self, arr):
		new_arr=[];
		for x in arr:
			if type(x) is list:
				new_arr=new_arr+self.extract(x);
			else:
				new_arr.append(x);
		return new_arr;
	
	#Description: Checks whether each data in the list is Null or None type
	#Returns: Boolean. True if all data in the list is Null or None type.
	def isNull(self, arr):
		raw_data=self.extract(arr);
		for data in raw_data:
			if data:
				return False;
		return True;

#Class Description: This class represent the collection of the skis of a mountain has
#Class Properties:
#1. ski = array representing the skis that can be found in the mountain
#2. width = the width of the array
#3. height = the height of the array		
class SkiManager:
	def __init__(self, w=0, h=0, *data):
		self.width=w;
		self.height=h;
		self.ski=[];
		self.ski_counter=0;
		ctr=0;
		for x in range(0, w):
			for y in range(0, h):
				new_ski=Ski(x, y, data[ctr]);
				if y>0:
					new_ski.left=self.ski[ctr-1];
					self.ski[ctr-1].right=new_ski;
				if x>0:
					tip=w*(x-1)+y;
					new_ski.top=self.ski[tip];
					self.ski[tip].bottom=new_ski;
				self.ski.append(new_ski);
				ctr=ctr+1;
    
	#Description: Reads data from a file
	#The first line should represent the height and the width of the array to be read from the file
	#The next proceeding lines will represent the ski's data
	def readFile(self, filename):
		file_data=open(filename);
		raw_data=file_data.readline();
		#CONFIG
		self.height, self.width=raw_data.split(' ');
		self.height=int(self.height);
		self.width=int(self.width);
		line_ctr, data_ctr=0, 0;
		#Start Reading/Seeding
		for x in range(0, self.height):
			raw_data=file_data.readline().split(' ');
			for y in range(0, self.width):
				if raw_data[y]:
					new_ski=Ski(x, y, int(raw_data[y]));
					print(new_ski);
					print('\n');
					if y>0:
						new_ski.left=self.ski[data_ctr-1];
						self.ski[data_ctr-1].right=new_ski;
					if x>0:
						tip=self.width*(x-1)+y;
						new_ski.top=self.ski[tip];
						self.ski[tip].bottom=new_ski;
					self.ski.append(new_ski);
					data_ctr=data_ctr+1;
			line_ctr=line_ctr+1;
		print('Width: {}, Height: {}'.format(self.width, self.height));
		print('Total Lines: {}'.format(line_ctr));
		
	
	#Description: Evaluate each skis and store their potential max value in a list and use max to get the highest maximum value
	#Returns: Ski object of the highest maximum potential value
	def maxSki(self):
		list_ski_value=[];
		for x in self.ski:
			list_ski_value.append(x.max_value);
		#print(list_ski_value);
		max_ski_value=max(list_ski_value);
		max_ski_index=list_ski_value.index(max_ski_value);
		return self.ski[max_ski_index];
	
	#Description: Evaluate a ski item to get its maximum possible drop, filename parameter is optional for logging the evaluations
	#in the external file as all entries cannot be displayed under cmd window.
	#Returns: Array of Skis to cross that will yield to highest maximum value of a given ski
	def maxDrop(self, ski_item, filename=None):
		max_drop_ski=[];
		next_drop=0;
		while not next_drop==-1:
			next_drop=ski_item.max_pointer;
			#print(ski_item.max_value);
			max_drop_ski.append(ski_item);
			if next_drop==0:
				ski_item=ski_item.top;
			elif next_drop==1:
				ski_item=ski_item.bottom;
			elif next_drop==2:
				ski_item=ski_item.left;
			elif next_drop==3:
				ski_item=ski_item.right;
		print('-------------------------------------');
		if filename:
			file_writer=open(filename, 'a');
			file_writer.write('-------------------------------------\n');
			for x in max_drop_ski:
				print(x);
				file_writer.write(x.__str__());
				file_writer.write('\n');
			file_writer.close();
		return max_drop_ski;
	
	#Description: Print out the skis stored in the ski array of this class
	def printSkis(self):
		for x in range(0, self.width):
			for y in range(0, self.height):
				print(str(self.ski[x*self.width+y]));
			print('\n');
	
	def eval(self, ski_item):
		#print('-------EVAL----------');
		#print(ski_item);
		if ski_item==None:
			return 0;
		elif ski_item and hasattr(ski_item, 'max_value'):
			return ski_item.max_value;
		else:
			ski_around=ski_item.get_around_ski();
			#print('--------AROUND-----------');
			#for ski in ski_around:
			#	print(ski);
			#	print('\n');
			sm.ski_counter=sm.ski_counter-1;
			if ski_item.isNull(ski_around):
				ski_item.max_value=ski_item.value;
				ski_item.max_pointer=-1;
				return ski_item.max_value;
			else:
				ski_max_vals=[];
				for ski in ski_around:
					ski_max_vals.append(self.eval(ski));
				ski_max=max(ski_max_vals);
				ski_item.max_value=ski_item.value+ski_max;
				ski_item.max_pointer=ski_max_vals.index(ski_max);
				return ski_item.max_value;
	
#Program Proper Execution
#Instatiate Ski Manager
sm=SkiManager();
#Read and seed data from a file
sm.readFile('map.txt');
#print(sm.eval(sm.ski[6]));
#print(sm.ski[10].max_value);
#print(sm.ski[15].max_value);
#Iterate over the seeded ski their potential maximum value
ctr=1;
sm.ski_counter=int(sm.height*sm.width);
for x in sm.ski:
	if not hasattr(x, 'max_value'):
		print('Evaluating {} of {}: {}\n'.format(ctr, sm.ski_counter, x.value));
		sm.eval(x);
	ctr=ctr+1;

#Max
max_ski_item=sm.maxSki();
print('--------MAX SKI ITEM----------------');
print(max_ski_item);
print('Max Value: {}'.format(max_ski_item.max_value));
print('Max Drop: {}'.format(len(sm.maxDrop(max_ski_item, 'max_drop_log.txt'))));
